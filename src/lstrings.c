/* lstrings.c */

/* Language-specific strings (English) */

/*
 *  ``The contents of this file are subject to the Mozilla Public License
 *  Version 1.0 (the "License"); you may not use this file except in
 *  compliance with the License. You may obtain a copy of the License at
 *  http://www.mozilla.org/MPL/
 *
 *  Software distributed under the License is distributed on an "AS IS"
 *  basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 *  License for the specific language governing rights and limitations
 *  under the License.
 *
 *  The Original Code is the "Light Speed!" relativistic simulator.
 *
 *  The Initial Developer of the Original Code is Daniel Richard G.
 *  Portions created by the Initial Developer are Copyright (C) 1999
 *  Daniel Richard G. <skunk@mit.edu> All Rights Reserved.
 *
 *  Contributor(s): ______________________________________.''
 */


#include "lightspeed.h"

#define N_(Text) Text

/* All changes necessary to port Light Speed! to a new language should be
 * contained within this file. If modifications are necessary elsewhere in
 * the code, please let me know. There are surely a few odd English-specific
 * constructs not quite in here yet :-) */


const char *STR_Light_Speed		= "Light Speed!";
/* %d = year, %s = author's name or author's name + e-mail address */
const char *STR_copyright_ARG		= N_("Copyright (C) %d by %s");

/**** COMMAND-LINE INTERFACE ****/

/* %s = executable name */
const char *STR_CLI_usage_ARG		= N_("usage: %s [-hsa] [object]");
struct option_desc STRS_CLI_options[] = {
	{ 'h', "help", N_("Print this help screen") },
#ifdef DEF_ADVANCED_INTERFACE
	{ 's', "simple", N_("Use simplified interface") },
	{ 'a', "advanced", N_("Use advanced interface (default)") },
#else
	{ 's', "simple", N_("Use simple interface (default)") },
	{ 'a', "advanced", N_("Use more advanced interface") },
#endif /* not DEF_ADVANCED_INTERFACE */
	{ '\0', N_("object"), N_("3D file to load on startup (.3DS or .LWO)") }
};
const char *STR_CLI_option_chars	= "hsa";

/**** MENUS ****/

/* Menu bar headings */
const char *STR_MNU_File		= N_("File");
const char *STR_MNU_Objects		= N_("Objects");
const char *STR_MNU_Warp		= N_("Warp");
const char *STR_MNU_Camera		= N_("Camera");
const char *STR_MNU_Help		= N_("Help");

/* File menu */
const char *STR_MNU_New_lattice		= N_("New lattice...");
const char *STR_MNU_Load_object		= N_("Load object...");
const char *STR_MNU_Save_snapshot	= N_("Save snapshot...");
const char *STR_MNU_Export_srs		= N_("Export SRS...");
const char *STR_MNU_Exit		= N_("Exit");

/* Objects menu */
const char *STR_MNU_Coordinate_axes	= N_("Coordinate axes");
const char *STR_MNU_Floating_grid	= N_("Floating grid");
const char *STR_MNU_Bounding_box	= N_("Bounding box");
const char *STR_MNU_Animation		= N_("Animation...");

/* Warp menu */
const char *STR_MNU_Lorentz_contraction	= N_("Lorentz contraction");
const char *STR_MNU_Doppler_shift	= N_("Doppler red/blue shift");
const char *STR_MNU_Headlight_effect	= N_("Headlight effect");
const char *STR_MNU_Optical_deformation	= N_("Optical aberration");

/* Camera menu */
const char *STR_MNU_Lens		= N_("Lens");
const char *STR_MNU_Position		= N_("Position...");
const char *STR_MNU_Reset_view		= N_("Reset view");
const char *STR_MNU_Info_display	= N_("Info display");
const char *STR_MNU_Background		= N_("Background");
const char *STR_MNU_Graphics_mode	= N_("Graphics mode");
const char *STR_MNU_Spawn_camera	= N_("Spawn camera");
const char *STR_MNU_Close		= N_("Close");

/* Camera->Lens submenu */
const char *STR_MNU_Custom		= N_("Custom");

/* Camera->Info_display submenu */
const char *STR_MNU_Active		= N_("Active");
const char *STR_MNU_Velocity		= N_("Velocity");
const char *STR_MNU_Time_t		= N_("Time t");
const char *STR_MNU_Gamma_factor	= N_("Gamma factor");
const char *STR_MNU_Framerate		= N_("Framerate");

/* Camera->Background submenu colors
 * (corresponding color hues are defined in globals.c) */
const char *STRS_MNU_bkgd_color_names[]	= { N_("Black"), N_("Grey"), N_("White"), N_("Very white") };

/* Camera->Graphics_mode submenu */
const char *STR_MNU_Wireframe		= N_("Wireframe");
const char *STR_MNU_Shaded		= N_("Shaded");

/* Help menu */
const char *STR_MNU_Overview		= N_("Overview");
const char *STR_MNU_Controls		= N_("Controls");
const char *STR_MNU_About		= N_("About");

/**** INFO DISPLAY ****/

/* Time t
 * %s == time string as returned by time_string( ) */
const char *STR_INF_time_ARG		= N_("t = %ssec");

/* Framerate */
const char *STR_INF_fps_ARG		= N_("%.1f fps");

/* Velocity
 * %s == velocity strings as returned by velocity_string( ???, TRUE ) */
const char *STR_INF_velocity_ARG	= N_("Velocity: %s");

/* Gamma factor */
const char *STR_INF_gamma_ARG		= N_("gamma = %.3f");

/* Relativistic toggle messages */
const char *STR_INF_no_contraction	= N_("LORENTZ CONTRACTION NOT SHOWN");
const char *STR_INF_no_doppler_shift	= N_("DOPPLER RED/BLUE SHIFT NOT SHOWN");
const char *STR_INF_no_headlight_effect	= N_("HEADLIGHT EFFECT NOT SHOWN");
const char *STR_INF_no_deformation	= N_("OPTICAL ABERRATION NOT SHOWN");
const char *STR_INF_no_relativity	= N_("NO RELATIVISTIC EFFECTS SHOWN!!!");

/**** DIALOGS ****/

const char *STR_DLG_Okay_btn		= N_("OK");
const char *STR_DLG_Cancel_btn		= N_("Cancel");
const char *STR_DLG_Close_btn		= N_("Close");

/* New Lattice dialog */
const char *STR_DLG_New_lattice		= N_("New lattice");
const char *STR_DLG_Dimensions		= N_("Dimensions");
const char *STR_DLG_Smoothness		= N_("Smoothness");

/* Load Object dialog */
const char *STR_DLG_Load_Object		= N_("Load Object");
const char *STR_DLG_Recognized_formats	= N_("Recognized formats");
const char *STR_DLG_3d_formats		= N_("3D Studio: *.3DS or *.PRJ\nLightWave 3D: *.LWO");

/* Save snapshot dialog */
const char *STR_DLG_Save_Snapshot	= N_("Save Snapshot");
const char *STR_DLG_snapshot_Parameters	= N_("Parameters");
const char *STR_DLG_snapshot_Size	= N_("Size");
const char *STR_DLG_snapshot_Format	= N_("Format");
const char *STR_DLG_snapshot_basename	= N_("snapshot");

/* Export SRS dialog */
const char *STR_DLG_Export_srs		= N_("Export SRS");
const char *STR_DLG_srs			= N_("Special Relativity Scene (SRS)");
const char *STR_DLG_srs_Parameters	= N_("Parameters");
const char *STR_DLG_srs_Size		= N_("Rendered size");
const char *STR_DLG_srs_Stereo_view	= N_("Stereoscopic view");
const char *STR_DLG_srs_Vis_faces_only	= N_("Visible faces only");
/* basename1 is for lattices, basename2 is for imported geometry */
const char *STR_DLG_srs_basename1	= N_("lattice");
const char *STR_DLG_srs_basename2	= N_("object");

/* Animation dialog */
const char *STR_DLG_Animation		= N_("Animation");
const char *STR_DLG_Observed_range	= N_("Observed range of motion");
const char *STR_DLG_Start_X		= N_("Starting X");
const char *STR_DLG_End_X		= N_("Ending X");
const char *STR_DLG_Loop_time		= N_("Loop time: ");
const char *STR_DLG_seconds		= N_(" seconds");
const char *STR_DLG_Begin_btn		= N_("Begin");
const char *STR_DLG_Stop_btn		= N_("Stop");

/* Camera Position dialog */
const char *STR_DLG_Camera_Position	= N_("Camera Position");
const char *STR_DLG_Location		= N_("Location");
const char *STR_DLG_View_target		= N_("View target");
const char *STR_DLG_Direction		= N_("Direction");
const char *STR_DLG_Phi_label		= N_("Phi [0, 360)");
const char *STR_DLG_Theta_label		= N_("Theta [-90, 90]");
const char *STR_DLG_Angles_instead	= N_("Specify phi/theta direction instead");
const char *STR_DLG_Xyz_instead		= N_("Specify (x,y,z) view target instead");
const char *STR_DLG_Reposition_btn	= N_("Reposition");

/* Custom Lens dialog */
const char *STR_DLG_Custom_Lens		= N_("Custom Lens");
const char *STR_DLG_Lens_length		= N_("Lens length");
const char *STR_DLG_Field_of_view	= N_("Field of view");
const char *STR_DLG_degree_suffix	= N_("deg");

/* Overview dialog */
const char *STR_DLG_Overview		= N_("Overview");

/* Controls dialog */
const char *STR_DLG_Controls		= N_("Controls");

/* About dialog */
const char *STR_DLG_About		= N_("About");
/* %d == version major, %d == version minor */
const char *STR_DLG_Version_x_y_ARG	= N_("Version %s");
/* %s == author's name */
const char *STR_DLG_authorship_ARG	= N_("A Theory Toy by %s");
const char *STR_DLG_home_page_url	= "http://lightspeed.sourceforge.net/";

/* Spawned camera window title */
const char *STR_DLG_Camera		= N_("Camera");

/* Miscellaneous dialog titles */
const char *STR_DLG_Warning		= N_("Warning");
const char *STR_DLG_Error		= N_("Error");

/* Help->Overview text
 * (from the OVERVIEW file) */
const char *STR_DLG_Overview_TEXT = N_("\
Light Speed! is a simulator which can illustrate the effects of\n\
special relativity on the appearance of objects travelling at\n\
ultra-high speeds. Depending on the particular speed, and one's\n\
point of view, relativistic effects can cause the object to\n\
appear shorter, longer, brighter, darker, deformed and/or\n\
off-color.\n\
\n\
To adjust the velocity, use the slider along the right edge of\n\
the main window. You can also type in a value, using the entry\n\
at the top right (press <Enter> once it is in). To change the\n\
units shown, press the button to the right of the entry, and it\n\
will cycle through a small list.\n\
\n\
The object travels in the positive-x direction, which can be\n\
visually checked by activating Objects -> Coordinate axes.\n\
");

/* Help->Controls text
 * (from the CONTROLS file) */
const char *STR_DLG_Controls_TEXT = N_("\
Most interactive control is performed with the mouse. By holding\n\
down a particular button, and dragging the pointer around,\n\
various camera motions can be obtained:\n\
\n\
   Left button:  Revolve camera around view targetg\n\
\n\
   Left button + Shift key:  Revolve view target around camera\n\
\n\
   Middle button:  Translate camera left, right, up or down\n\
\n\
   Right button:  Dolly in or out\n\
\n\
The first and last motions are generally the most useful.\n\
Should the camera become difficult to control at any point, it\n\
may be re-initialized by selecting Camera -> Reset View.\n\
");

/**** MESSAGES ****/

/* For quick-and-dirty command feedback */
const char *STR_MSG_Okay		= N_("OK");
const char *STR_MSG_Invalid		= N_("INVALID");

/* File overwrite warning
 * %s == filename (basename) of file at risk */
const char *STR_MSG_overwrite_warn_ARG	= N_("The file \"%s\" already exists.\nPress OK to overwrite it.");

/* Object importer error messages */
/* %s == (bogus) filename */
const char *STR_MSG_no_object_file_ARG	= N_("The specified object file could not be opened.\n(%s)\nImport operation failed.");
const char *STR_MSG_not_3ds_file	= N_("The file lacks a valid 3DS signature.\nImport operation failed.");
const char *STR_MSG_not_prj_file	= N_("The file lacks a valid 3D Studio PRJ signature.\nImport operation failed.");
const char *STR_MSG_not_lwo_file	= N_("The file lacks a valid LWOB signature.\nImport operation failed.");
const char *STR_MSG_unknown_obj_format	= N_("The object must be in 3D Studio (3DS)\nor LightWave 3D (LWO) file format.\nImport operation failed.");
const char *STR_MSG_bad_3ds_file	= N_("The 3D Studio file could not be properly read.\nImport operation failed.");
const char *STR_MSG_empty_3ds_file	= N_("The 3D Studio file has no valid geometry.\nImport operation failed.");
const char *STR_MSG_bad_lwo_file	= N_("The LightWave file could not be properly read.\nImport operation failed.");
const char *STR_MSG_empty_lwo_file	= N_("The LightWave file has no valid geometry.\nImport operation failed.");

/* Snapshot exporter error messages */
const char *STR_MSG_no_ogl_visual	= N_("The system could not provide the required visual.\nSave operation failed.");
/* %d == width, %d == height */
const char *STR_MSG_no_render_buf_ARG	= N_("A %dx%d render buffer could not be allocated.\nSave operation failed.");
const char *STR_MSG_no_ogl_context	= N_("The required OpenGL context was not available.\nSave operation failed.");
const char *STR_MSG_no_snapshot_output	= N_("An error occurred in creating the output file.\nSave operation failed.");

/* Viewport-centered messages */
const char *STR_MSG_Generating_lattice	= N_("GENERATING LATTICE . . .");
const char *STR_MSG_Importing_object	= N_("LOADING OBJECT . . .");
const char *STR_MSG_Rendering_snapshot	= N_("RENDERING . . .");
/* %d == percent of snapshot written */
const char *STR_MSG_Saving_snapshot_ARG	= N_("SAVING SNAPSHOT . . .\n(%d%%)");

/* end lstrings.c */
