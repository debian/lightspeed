#include <pango/pangoft2.h>

/* Mennucc:
   how do we use fonts in GL ? simple, we don't !

   this file contains code from gtkglext  taken from:

 * example/font-pangoft2.c:
 * Simple example for text rendering with PangoFT2.
 * License: LGPL
 * Copyright: written by Naofumi Yasufuku  <naofumi@users.sourceforge.net>

 this code is currently unused and is probably not working;but it would support UTF-8

 */

static gboolean
gl_tex_pango_ft2_render_layout (PangoLayout *layout,
                                GLuint *texture,
                                GLfloat *s0,
                                GLfloat *s1,
                                GLfloat *t0,
                                GLfloat *t1)
{
  PangoRectangle logical_rect;
  FT_Bitmap bitmap;
  guint32 *t;
  GLfloat color[4];
  guint32 rgb;
  GLfloat a;
  guint8 *row, *row_end;
  int i;

  if (text_texture.size == 0)
    return FALSE;

  pango_layout_get_extents (layout, NULL, &logical_rect);
  if (logical_rect.width == 0 || logical_rect.height == 0)
    return FALSE;

  bitmap.rows = PANGO_PIXELS (logical_rect.height);
  bitmap.width = PANGO_PIXELS (logical_rect.width);

  /* Ad hoc :-p */
  if (bitmap.width > text_texture.size || bitmap.rows > text_texture.size)
    return FALSE;

  bitmap.pitch = bitmap.width;
  bitmap.buffer = g_malloc (bitmap.rows * bitmap.width);
  bitmap.num_grays = 256;
  bitmap.pixel_mode = ft_pixel_mode_grays;
  memset (bitmap.buffer, 0, bitmap.rows * bitmap.width);
  pango_ft2_render_layout (&bitmap, layout,
                           PANGO_PIXELS (-logical_rect.x), 0);

  glGetFloatv (GL_CURRENT_COLOR, color);
#if !defined(GL_VERSION_1_2) && G_BYTE_ORDER == G_LITTLE_ENDIAN
  rgb =  ((guint32) (color[0] * 255.0))        |
        (((guint32) (color[1] * 255.0)) << 8)  |
        (((guint32) (color[2] * 255.0)) << 16);
#else
  rgb = (((guint32) (color[0] * 255.0)) << 24) |
        (((guint32) (color[1] * 255.0)) << 16) |
        (((guint32) (color[2] * 255.0)) << 8);
#endif
  a = color[3];

  row = bitmap.buffer + bitmap.rows * bitmap.width; /* past-the-end */
  row_end = bitmap.buffer;      /* beginning */

  t = (guint32 *) text_texture.texels;

  if (a == 1.0)
    {
      do
        {
          row -= bitmap.width;
          for (i = 0; i < bitmap.width; i++)
#if !defined(GL_VERSION_1_2) && G_BYTE_ORDER == G_LITTLE_ENDIAN
            *t++ = rgb | (((guint32) row[i]) << 24);
#else
            *t++ = rgb | ((guint32) row[i]);
#endif
        }
      while (row != row_end);
    }
  else
    {
      do
        {
          row -= bitmap.width;
          for (i = 0; i < bitmap.width; i++)
#if !defined(GL_VERSION_1_2) && G_BYTE_ORDER == G_LITTLE_ENDIAN
            *t++ = rgb | (((guint32) (a * row[i])) << 24);

#else
            *t++ = rgb | ((guint32) (a * row[i]));
#endif
        }
      while (row != row_end);
    }

  glPixelStorei (GL_UNPACK_ALIGNMENT, 4);

  glBindTexture (GL_TEXTURE_2D, text_texture.name);
#if !defined(GL_VERSION_1_2)
  glTexSubImage2D (GL_TEXTURE_2D, 0,
                   0, 0, bitmap.width, bitmap.rows,
                   GL_RGBA, GL_UNSIGNED_BYTE,
                   text_texture.texels);
#else
  glTexSubImage2D (GL_TEXTURE_2D, 0,
                   0, 0, bitmap.width, bitmap.rows,
                   GL_RGBA, GL_UNSIGNED_INT_8_8_8_8,
                   text_texture.texels);
#endif

  *texture = text_texture.name;

  *s0 = 0.0;
  *s1 = (GLfloat) bitmap.width / (GLfloat) text_texture.size;

  *t0 = 0.0;
  *t1 = (GLfloat) bitmap.rows / (GLfloat) text_texture.size;

  g_free (bitmap.buffer);

  return TRUE;
}

void ogl_draw_string_gtk2(const camera *cam, const gchar *text,int pos_code)
{ 
  g_return_if_fail(cam && text && cam->ogl_w);
  g_return_if_fail(GDK_IS_DRAWABLE (cam->ogl_w->window));

  //pixmap = gdk_pixmap_new( cam->ogl_w->window, width, height, -1);


  PangoLayout *layout = gtk_widget_create_pango_layout (cam->ogl_w->window, text);
  //PangoContext *context
  PangoFontDescription *fontdesc = pango_font_description_from_string ("Sans 12");
  //gdk_draw_layout (..., layout);  pango_layout_set_font_description (layout, fontdesc); 
  //PangoLayout *layout = pango_layout_new (ft2_context);
  pango_layout_set_font_description (layout, fontdesc); 
  pango_layout_set_width (layout, PANGO_SCALE * 200);
  pango_layout_set_alignment (layout, PANGO_ALIGN_CENTER);

  gdk_draw_layout (cam->ogl_w  ,
		   cam->ogl_w->style->fg_gc[0],
		   cam->width/2, cam->height/2,layout);
  return;

  //pango_layout_set_text (layout, text, -1);
  GLuint texture;
  GLfloat s0, s1, t0, t1;
  gboolean ret = gl_tex_pango_ft2_render_layout (layout,
                                        &texture,
                                        &s0, &s1, &t0, &t1);
  if (ret)
    {
      glPushMatrix ();
      //glTranslatef (0.0, -text_z * TANGENT, text_z + 2.0);
      //  glRotatef (ANGLE, 1.0, 0.0, 0.0);

        glEnable (GL_TEXTURE_2D);
        glEnable (GL_BLEND);
        glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

        glBindTexture (GL_TEXTURE_2D, texture);
        glTexEnvi (GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
        glBegin (GL_QUADS);
          glTexCoord2f (s0, t0); glVertex3f (-1.0, 0.0,  1.0);
          glTexCoord2f (s0, t1); glVertex3f (-1.0, 0.0, -1.0);
          glTexCoord2f (s1, t1); glVertex3f ( 1.0, 0.0, -1.0);
          glTexCoord2f (s1, t0); glVertex3f ( 1.0, 0.0,  1.0);
        glEnd ();

        glDisable (GL_BLEND);
        glDisable (GL_TEXTURE_2D);

      glPopMatrix ();
    }


  

  pango_font_description_free (fontdesc);
  g_object_unref (layout);
}



void
ogl_draw_string( const void *data, int message, int size )
{
  static camera *cam=NULL;
  switch (message) {
  case INITIALIZE:
    break;
  case RESET:
    cam = (camera *)data;
    break;
  default: {
    char *text = (char *)data; 
    ogl_draw_string_gtk2(cam, text, message);
  }
  }
}
